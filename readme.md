# Earliest Sunrise Script

This is a script that generates a set of random points on earth and finds the sunrise and day
length times at each, on the current date. It then prints out details of the location with the
earliest sunrise.

## Requirements

Node and NPM. Tested with node 14.7.0 and npm 6.14.7.

## Installation and building

As simple as:

```
npm install
npm run build
```

## Usage

```
Usage: node dist/bin/sunrise [options]

Options:
  -n, --num-points <N>   number of random points (default: "5")
  -w, --num-workers <N>  number of concurrent requests (default: "5")
  -h, --help             display help for command
```

## Tests

There are some limited tests, using the jest library. To run them:

```
npm run tests
```

## Notes

- "Earliest sunrise" depends on whether you're considering the sunrise time in local time,
  or global time (e.g. UTC). This script takes the simpler approach, and finds the location
  where sunrise time in UTC is earliest - i.e. the place where the sun first rises.
- In some places the sun does not rise at all at certain times of year. This means that sometimes
  the script cannot find an earliest location. In this case it prints an error and exits with
  status code 1.

## Limitations:

- Error handling is pretty limited. Ideally more specific errors should be caught and handled
  separately.
- The tests don't cover a lot of corner cases that they should cover, given more time.
- I have used ramda (for the first time!), but would describe the script as more "functionally inspired"
  then a truly functional program ;)