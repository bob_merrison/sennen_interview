import axios from 'axios';
import { AxiosInstance } from 'axios';
import * as R from 'ramda';

interface Coords {
    lat: number,
    long: number
};

interface TimeInfo {
    sunrise: string,
    sunset: string,
    solar_noon: string,
    day_length: number,
    civil_twilight_begin: string,
    civil_twilight_end: string,
    nautical_twilight_begin: string,
    nautical_twilight_end: string,
    astronomical_twilight_begin: string,
    astronomical_twilight_end: string
}

interface LocationInfo extends Coords {
    timeInfo: TimeInfo
};

interface ApiResponse {
    results: TimeInfo
};

async function apiWorker(client: AxiosInstance, nextItem: () => Coords, onResult: (l: LocationInfo) => void)
    : Promise<void>
{
    for (let item = nextItem(); item; item = nextItem()){
        await client
            .get<ApiResponse>(`json?lat=${item.lat}&lng=${item.long}&formatted=0`)
            .then(response => {
                onResult({lat: item.lat, long: item.long, timeInfo: response.data.results});
            })
        ;
    }
}

async function locationInfo(latLongs: Coords[], numWorkers: number)
    : Promise<LocationInfo[]>
{
    const apiClient = axios.create({
        baseURL: 'https://api.sunrise-sunset.org/',
        responseType: 'json'
    });
    const queue = Array.from(latLongs);
    const results: LocationInfo[] = [];
    const createWorker = R.partial(
        apiWorker,
        [apiClient, () => queue.pop(), (result: LocationInfo) => results.push(result)]
    );
    const workers = R.times(createWorker, numWorkers);
    await Promise.all(workers);
    return results;
}

export interface Result {
    numPoints: number,
    sunrise: string,
    lat: number,
    long: number,
    dayLength: number
};

export async function getEarliest(numPoints: number, numWorkers: number): Promise<Result>{
    if (numPoints <= 0) {
        throw new RangeError('Number of points must be >= 0.');
    }
    if (numWorkers <= 0) {
        throw new RangeError('Number of workers must be >= 0.');
    }
    const randRange = (min: number, max: number) => min + (max - min) * Math.random();
    const randLatLong = () => ({ lat: randRange(-90, 90), long: randRange(-180, 180)});
    const coords = R.times(randLatLong, numPoints);
    const times = await locationInfo(coords, numWorkers)
        .catch(err => { throw new Error(`Error getting times from API: ${err.message}.`)})
    ;
    const hasSunrise = (time: LocationInfo) => time.timeInfo.day_length !== 0
    const filtered = R.filter(hasSunrise, times);
    if (filtered.length === 0){
        throw new Error('No sampled sites have a sunrise.');
    }
    const earlierSunrise = (a: LocationInfo, b: LocationInfo) => a.timeInfo.sunrise < b.timeInfo.sunrise;
    const earliestSunrise = R.reduce(
        (acc: LocationInfo, item: LocationInfo) => earlierSunrise(item, acc) ? item : acc,
        filtered[0],
        filtered.slice(1)
    );
    return {
        numPoints: numPoints, 
        lat: earliestSunrise.lat,
        long: earliestSunrise.long,
        sunrise: earliestSunrise.timeInfo.sunrise,
        dayLength: earliestSunrise.timeInfo.day_length
    };
}

