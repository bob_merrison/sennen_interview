import * as sunrise from '../dist/src/sunrise-lib';
import axios from 'axios';

const mockGet = jest.fn();
const mockAxiosInstance = {
    get: () => new Promise(accept => { accept(mockGet()); })
};
const failureAxiosInstance = {
    get: () => new Promise((accept, reject) => { reject(mockGet()); })
}

jest.mock('axios');
beforeEach(() => {
    mockGet.mockReset();
    axios.create.mockImplementation(() => mockAxiosInstance);
});

describe('getEarliest', () => {
    it('returns an error when given a negative number of points', () => {
        return sunrise.getEarliest(-1, 1).catch(e => expect(e).toBeInstanceOf(RangeError));
    });
    it('returns an error when given zero points', () => {
        return sunrise.getEarliest(0, 1).catch(e => expect(e).toBeInstanceOf(RangeError));
    });
    it('returns an error when given a negative number of workers', () => {
        return sunrise.getEarliest(1, -1).catch(e => expect(e).toBeInstanceOf(RangeError));
    });
    it('returns an error when given zero workers', () => {
        return sunrise.getEarliest(1, 0).catch(e => expect(e).toBeInstanceOf(RangeError));
    });
    it('succeeds when called with numPoints and numWorkers = 1', async () => {
        mockGet.mockReturnValueOnce({
            data: {
                results: {
                    sunrise: '2021-10-08T07:00:00+00:00',
                    day_length: 10
                }
            }
        });
        const result = await sunrise.getEarliest(1, 1);
        expect(mockGet).toHaveBeenCalledTimes(1);
        expect(result).toHaveProperty('sunrise', '2021-10-08T07:00:00+00:00');
        expect(result).toHaveProperty('dayLength', 10);
        expect(result).toHaveProperty('numPoints', 1);
        expect(result.lat).toEqual(expect.any(Number));
        expect(result.long).toEqual(expect.any(Number));
    });
    it('returns the first of two locations when it has the earliest sunrise', async () => {
        mockGet
            .mockReturnValueOnce({
                data: {
                    results: {
                        sunrise: '2021-10-08T07:00:00+00:00',
                        day_length: 10
                    }
                }
            })
            .mockReturnValueOnce({
                data: {
                    results: {
                        sunrise: '2021-10-08T08:00:00+00:00',
                        day_length: 20
                    }
                }
            })
        ;
        const result = await sunrise.getEarliest(2, 1);
        expect(mockGet).toHaveBeenCalledTimes(2);
        expect(result).toHaveProperty('sunrise', '2021-10-08T07:00:00+00:00');
        expect(result).toHaveProperty('dayLength', 10);
        expect(result).toHaveProperty('numPoints', 2);
    });
    it('returns the second of two locations when it has the earliest sunrise', async () => {
        mockGet
            .mockReturnValueOnce({
                data: {
                    results: {
                        sunrise: '2021-10-08T08:00:00+00:00',
                        day_length: 10
                    }
                }
            })
            .mockReturnValueOnce({
                data: {
                    results: {
                        sunrise: '2021-10-08T07:00:00+00:00',
                        day_length: 20
                    }
                }
            })
        ;
        const result = await sunrise.getEarliest(2, 1);
        expect(mockGet).toHaveBeenCalledTimes(2);
        expect(result).toHaveProperty('sunrise', '2021-10-08T07:00:00+00:00');
        expect(result).toHaveProperty('dayLength', 20);
        expect(result).toHaveProperty('numPoints', 2);
    });
    it('is rejected with an error when the get call is rejected', () => {
        axios.create.mockImplementation(() => failureAxiosInstance);
        mockGet.mockReturnValueOnce(new Error('Mock error'));
        return expect(sunrise.getEarliest(1, 1))
            .rejects.toEqual(new Error('Error getting times from API: Mock error.'))
        ;
    });
    it('is rejected when no locations have a sunrise', () => {
        mockGet.mockReturnValueOnce({
            data: {
                results: {
                    day_length: 0
                }
            }
        });
        return expect(sunrise.getEarliest(1, 1))
            .rejects.toEqual(new Error('No sampled sites have a sunrise.'))
        ;
    });
});