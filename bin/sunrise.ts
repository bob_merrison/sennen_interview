import {getEarliest} from '../src/sunrise-lib';
import type {Result} from '../src/sunrise-lib';

const { program } = require('commander');
program
    .option('-n, --num-points <N>', 'number of random points', '5')
    .option('-w, --num-workers <N>', 'number of concurrent requests', '5')
;
const options = program.parse(process.argv).opts();

getEarliest(options.numPoints, options.numWorkers)
    .catch((err: Error) => {
        console.log(err.message);
        process.exit(1);
    })
    .then((result: Result) => {
        console.log(`Earliest sunrise of ${result.numPoints} random points is ${result.sunrise}.`);
        console.log(`It occurs at ${result.lat},${result.long}.`);
        console.log(`Here, the day is ${result.dayLength} seconds long.`);
    });
;